#!/bin/bash
VCFDIR=$1
PHENO=$2
SNPINFODIR=$3
OUTDIR=$4
PROJECT=${5-PRJ}

scriptDir=$( cd $(dirname "$0") && pwd )
SEQMETA="python ${scriptDir}/seqmeta_pipeline.py"

if [ $# -lt 4 ]; then
   echo "USAGE: $0 <vcf_dir> <phenotype_file> <snp_info_dir> <out_dir> <project_tag>"
   echo "NOTE: <vcf_dir> should contain chr<chrNum>.dose.vcf.gz per-chromosome VCF files"
   echo "Output will be written to <out_dir>/chr<chrNum> subfolders for each chromosome"
   echo "IMPORTANT: subject IDs in <phenotype_file> should match subject IDs in VCF files in the same order!"
   exit 1
fi

# change these limits accordingly
maxcpus_job=4

chrList="$(seq 1 22)"
# iterate over chromosome list sequentially: chr1, chr2, ..., chr22
for chrNum in ${chrList}; do
  vcf="${VCFDIR}/chr${chrNum}.dose.vcf.gz"
  if [ ! -s ${vcf} ]; then
    echo "ERROR: $vcf not found. Exiting..."
    exit 1
  fi
  jobdir="${OUTDIR}"
  mkdir -p "${jobdir}"
  cmd=(${SEQMETA} -w ${jobdir} --cpu ${maxcpus_job} --project "${PROJECT}" ${vcf} ${SNPINFODIR} ${PHENO} ${chrNum})
  echo "${cmd[@]}"
  "${cmd[@]}"
done
