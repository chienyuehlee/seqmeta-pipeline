#!/bin/bash
set -eu

GENDIR=$1
PHENO=$2
SNPINFODIR=$3
OUTDIR=$4
PROJECT=${5-PRJ}

scriptDir=$( cd $(dirname "$0") && pwd )
SEQMETA="python ${scriptDir}/seqmeta_pipeline.py"

if [ $# -lt 4 ]; then
   echo "USAGE: $0 <GEN_dir> <phenotype_file> <snp_info_dir> <out_dir> <project_tag>"
   echo "NOTE: <GEN_dir> should contain chr<chrNum>.gen.gz per-chromosome GEN files"
   echo "Output will be written to <out_dir>/chr<chrNum> subfolders for each chromosome"
   echo "IMPORTANT: subject IDs in <phenotype_file> should match subject IDs in GEN files in the same order!"
   exit 1
fi

# change these limits accordingly
maxcpus_job=4
maxmem_job=4096 # MB

# iterate over chromosomes
for chrNum in {1..22}; do
  jobdir="${OUTDIR}/chr$chrNum"
  genfile="${GENDIR}/chr${chrNum}.gen.gz"
  if [ ! -s "${genfile}" ]; then
    echo "ERROR: $genfile not found. Exiting..."
    exit 1
  fi
  jobname="seqmeta_gen_chr$chrNum"
  logdir="${jobdir}/log"
  mkdir -p "${logdir}"
  jobout="${logdir}/${jobname}.out"
  joberr="${logdir}/${jobname}.error"
  bsubcmd=(bsub -J ${jobname} -o ${jobout} -e ${joberr} -M ${maxmem_job} ${SEQMETA} -w ${jobdir} -g ${genfile} --cpu ${maxcpus_job} --project "${PROJECT}" ${SNPINFODIR} ${PHENO} ${chrNum})
  echo "${bsubcmd[@]}"
  "${bsubcmd[@]}"
done
